var database = require('./database')
var usuario = {}

usuario.selectAll = function (callback) {
  if (database){
    database.query('SELECT * FROM usuario', function(err, rows){
      if(err){
        throw err
      } else{
        callback(null, rows)
      }
    })
  }
}

usuario.select = function (idUsuario, callback){
  if(database){
    var consulta = 'SELECT * FROM USUARIO WHERE idUsuario = ?'
    database.query(consulta, idUsuario, function(err, rows){
      if (err){
        throw err
      } else {
        callback(null, rows)
      }
    })
  }
}

usuario.insert = function (data, callback){
  if (database){
    var consulta = 'CALL agregar_usuario(?)'
    var datos = [data.Nick, data.Contrasena]
    database.query(consulta, datos, function(err, rows){
      if (err){
        throw err
      } else {
        callback(null, {"insertId": rows.insertId})
      }
    })
  }
}

usuario.update = function (data, callback){
  if (database) {
    var consulta = 'CALL modificar_usuario(?)'
    var datos = [data.idUsuario, data.Nick, data.Contrasena]
    database.query(consulta, datos, function(err, rows){
      if (err){
        throw err
      } else {
        callback(null, data)
      }
    })
  }
}

usuario.remove = function(idUsuario, callback){
  if (database){
    var consulta = 'CALL eliminar_usuario(?)'
    database.query(consulta, idUsuario, function(err, rows){
      if (err){
        throw err
      } else {
        callback(null, {'Mensaje': "Se elimino el usuario"})
      }
    })
  }
}

usuario.login = function(data, callback) {
  if(database){
    var consulta = 'SELECT * FROM Usuario where Nick = ? AND Contrasena = ?'
    var datos = [data.Nick, data.Contrasena]
    database.query(consulta, datos, function(err, rows){
      if (err){
        throw err
      } else {
        callback(null, rows)
      }
    })
  }
}

module.exports = usuario
