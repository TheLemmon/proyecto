var express = require('express');
var router = express.Router();
var usuario = require('../model/usuario')

//login

router.post('/api/login', function(req, res){
  var data = {
    Nick : req.body.Nick,
		Contrasena: req.body.Contrasena
  }
  usuario.login(data, function(err, result){
    var resultado = JSON.stringify(result[0])
    if (resultado != null){
      // redireccionar
    res.redirect('/login')
    }
  })
});

// obtener todos
router.get('/api/usuario', function(req, res) {
  usuario.selectAll(function(err, result){
		typeof result !== undefined ?
			(res.json(result)) : (res.json({'Mensaje': 'no hay usuarios'}))
  });
});

// obtener uno
router.get('/api/usuario/:idUsuario', function(req, res) {
  var idUsuario = req.params.idUsuario
  usuario.select(idUsuario, function(error, resultado){
    if (typeof resultado !== undefined) {
      res.json(resultado)
    } else {
      res.json({"Mensaje": "No hay usuarios"})
    }
  })
})

//agregar
router.post('/api/usuario', function(req, res) {
  var data = {
    idUsuario : null,
    Nick : req.body.Nick,
		Contrasena: req.body.Contrasena
  }
  usuario.insert(data, function(error, resultado){
    if (resultado && resultado.insertId > 0) {
      var idUsuario = resultado.insertId
      res.json("api/usuario/" + idUsuario)
    } else {
      res.json({"Mensaje": "No hay usuarios"})
    }
  })
})

// modificar_usuario
router.put('/api/usuario/:idUsuario', function(req, res) {
  var idUsuario = req.params.idUsuario
  var data = {
    idUsuario : req.body.idUsuario,
		Nick : req.body.Nick,
		Contrasena: req.body.Contrasena
  }
    usuario.update(data, function(error, resultado) {
      if (resultado !== undefined) {
        res.json(resultado)
      } else {
        res.json({"Mensaje" : "No se modifico el usuario"})
      }
    })
})

// eliminar_usuario

router.delete('/api/usuario/:idUsuario', function(req, res) {
  var idUsuario = req.params.idUsuario;
  usuario.delete(idUsuario, function(error, resultado){
    if (typeof resultado !== undefined) {
      res.json(resultado);
    } else {
      res.json({"Mensaje": "No se elimino el usuario"});
    }
  });
});

module.exports = router;
