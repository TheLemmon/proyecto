var viewModel = {
	Nick: ko.observable(),
	Pass: ko.observable(),

	login: function () {
		var usuario = {
			Nick: viewModel.Nick(),
			Contrasena: viewModel.Pass()
		}
		viewModel.ajaxHelper('/api/login', 'POST', usuario);
	},

	agregar: function(){
		var usuario = {
			Nick: viewModel.Nick(),
			Contrasena: viewModel.Pass()
		}
		viewModel.ajaxHelper('api/registrar', 'POST', usuario)
	},

	ajaxHelper: function (uri, method, data) {
        $.ajax({
            type: method,
            url: uri,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null
        }).fail(function (jqXHR, textStatus, errorThrown) {
            main.error(errorThrown);
        });
    }
}

$(document).ready(function(){
	ko.applyBindings(viewModel);
});
